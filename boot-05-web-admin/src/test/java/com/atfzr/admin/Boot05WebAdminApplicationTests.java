package com.atfzr.admin;

import com.atfzr.admin.bean.User;
import com.atfzr.admin.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//org.junit.Test（这是JUnit4版本的）
import org.springframework.data.redis.core.ClusterOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;


@Slf4j
@SpringBootTest
class Boot05WebAdminApplicationTests {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    @Autowired
    UserMapper userMapper;

//    @Autowired
//    StringRedisTemplate redisTemplate;

    //@Transactional 标注后连接数据库有回滚功能
    @Test
    void contextLoads() {
//        jdbcTemplate.queryForObject("select * from account_tbl");
//        jdbcTemplate.queryForList("select * from account_tbl");
        Long aLong = jdbcTemplate.queryForObject("select count(*) from account_tbl", Long.class);
        log.info("记录总数:" + aLong);

        log.info("数据源类型{}:" , dataSource.getClass());
    }

    @Test
    void testUserMapper(){
        User user = userMapper.selectById(1);
        log.info("用户信息{}",user);
    }
//
//    @Test
//    void testRedis(){
//        ValueOperations<String, String> operations = redisTemplate.opsForValue();
//
//      operations.set("hello","world");
//
//        String hello = operations.get("hello");
//        System.out.println(hello);
//    }

}

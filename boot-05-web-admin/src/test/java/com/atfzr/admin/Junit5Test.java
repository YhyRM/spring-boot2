package com.atfzr.admin;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author silence
 * @date 2022/11/20---15:48
 * @SuppressWarnings({"all"})
 */
@DisplayName("junit5功能测试类")
public class Junit5Test {

    @Autowired
    JdbcTest jdbcTest;

    /**
     * 测试前置条件
     */
    @Test
    @DisplayName("测试前置条件")
    void testassumptions(){
        Assumptions.assumeTrue(true,"结果不是true");
        System.out.println("1654");
    }

    @Test
    @DisplayName("组合断言")
    void all() {
        /**
         * 所有断言都需要成功
         */
        assertAll("test",
                () -> assertTrue(true && true, "结果不为true"),
                () -> assertEquals(1, 1, "结果不相等"));

        System.out.println("=========");
    }

    @Test
    @DisplayName("快速失败")
    public void shouldFail() {
        fail("This should fail");
    }

    @Test
    @DisplayName("异常测试")
    public void exceptionTest() {
        ArithmeticException exception = Assertions.assertThrows(
                //扔出断言异常, 断定业务逻辑一定出现异常
                ArithmeticException.class, () -> System.out.println(1 % 0), "业务逻辑居然正常运行??");
    }


    /**
     * 断言：前面断言失败，【后面的代码】都不会执行
     */
    @DisplayName("测试简单断言")
    @Test
    void testSimpleAssertions() {
        int cal = cal(2, 5);
        assertEquals(8, cal, "业务逻辑计算失败");

        //assertSame(), 判断是否为同一个对象
//        assertArrayEquals(); 数组断言
    }

    int cal(int i, int j) {
        return i + j;
    }

    @DisplayName("测试displayname注解")
    @Test
    void testDisplayName() {
        System.out.println(1);
    }

    //禁用标志
    @Disabled
    @DisplayName("测试方法2")
    @Test
    void test2() {
        System.out.println(2);
    }

    //重复测试
    @RepeatedTest(5)
    @Test
    void test3() {
        System.out.println(5);
    }

    /**
     * 规定方法超时时间。超出时间测试出异常
     *
     * @throws InterruptedException
     */
    @Timeout(value = 500, unit = TimeUnit.MILLISECONDS)
    @Test
    void testTimeout() throws InterruptedException {
        Thread.sleep(600);
    }

    //========以下功能和AOP有点相似========

    /*============每一个测试之前之后都会执行============*/
    @BeforeEach
    void beforeEach() {
        System.out.println("测试就要开始了。。。");
    }

    @AfterEach
    void afterEach() {
        System.out.println("测试就要结束了。。。");
    }
    /*============每一个测试之前之后都会执行============*/


    /*============每一个测试之前之后只会执行一次============*/
    @BeforeAll
    static void beforeAll() {
        System.out.println("所有测试就要开始了。。。");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("所有测试就要结束了。。。");
    }
    /*============每一个测试之前之后只会执行一次============*/
}

package com.atfzr.admin.servlet;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @author silence
 * @date 2022/11/18---16:19
 * @SuppressWarnings({"all"})
 */
@Slf4j
// 单* 是servlet的写法， 双** 是spring的写法。 意思都是所有的通配符
@WebFilter(urlPatterns = {"/css/*", "/images/*"})
public class MyFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("MyFilter初始化完成");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("MyFilter工作");
        filterChain.doFilter(servletRequest, servletResponse );
    }

    @Override
    public void destroy() {
        log.info("MyFilter销毁");
    }
}

package com.atfzr.admin.mapper;

import com.atfzr.admin.bean.City;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

/**
 * @author silence
 * @date 2022/11/19---15:43
 * @SuppressWarnings({"all"})
 */
//@Mapper
public interface CityMapper {

    @Select("select * from city where id = #{id}")
    public City getById(Long id);

//    @Insert(" insert into city(`name`,`state`,`country`) values (#{name},#{state},#{country})")
//    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(City city);
}

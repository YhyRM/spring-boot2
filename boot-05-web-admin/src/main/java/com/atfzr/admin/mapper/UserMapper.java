package com.atfzr.admin.mapper;

import com.atfzr.admin.bean.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author silence
 * @date 2022/11/19---20:07
 * @SuppressWarnings({"all"})
 */

@Mapper
public interface UserMapper extends BaseMapper<User> {

}

package com.atfzr.admin.mapper;

import com.atfzr.admin.bean.Account;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author silence
 * @date 2022/11/19---13:59
 * @SuppressWarnings({"all"})
 */
//@Mapper
public interface AccountMapper {

    public Account getAcct(Long id);
}

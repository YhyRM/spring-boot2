package com.atfzr.admin.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author silence
 * @date 2022/11/18---15:03
 * @SuppressWarnings({"all"})
 */

/**
 *  处理整个web controller 异常
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     *     @ExceptionHandler 指定要处理的异常
     * @param e
     * @return
     */
    @ExceptionHandler({ArithmeticException.class,NullPointerException.class})  //处理异常
    public String handleArithException(Exception e){

        log.error("异常是: {}" , e);

        return "login";
    }
}

package com.atfzr.admin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author silence
 * @date 2022/11/18---15:19
 * @SuppressWarnings({"all"})
 */

/**
 *   @ResponseStatus 自定义异常
 */
@ResponseStatus(value= HttpStatus.FORBIDDEN, reason = "用户数量太多")
public class UserTooManyException extends  RuntimeException{

    public UserTooManyException() {

    }
    public UserTooManyException(String message) {
        super(message);
    }
}

package com.atfzr.admin.config;

import com.atfzr.admin.interceptor.LoginInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author silence
 * @date 2022/11/17---16:35
 * @SuppressWarnings({"all"})
 */

/**  拦截步骤:
 *  1、 编写一个拦截器实现HandlerInterceptor接口
 *  2、 拦截器注册到容器(配置)中 (实现WebMvcConfigurer的addInterceptors)
 *  3、 指定拦截规则【如果是拦截所有，静态资源(样式，包等)也会被拦截】
 *
 * @EnableWebMvc 全面接管
 *  `@EnableWebMvc` + `WebMvcConfigurer` — `@Bean`  可以全面接管SpringMVC，所有规则全部自己重新配置；
 *    实现定制和扩展功能（**高级功能，初学者退避三舍**）。
 */
//@EnableWebMvc
@Configuration
public class AdminWebConfig implements WebMvcConfigurer {

//    /**
//     *  定义静态资源行为
//     * @param registry
//     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//        /**
//         *  访问 /aa/** 所有请求都去  classpath:/static 下面进行匹配d
//         */
//        registry.addResourceHandler("/aa/**")
//                .addResourceLocations("classpath:/static");
//
//    }
//
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**") //所有请求都会被拦截，包括静态资源(样式)
                .excludePathPatterns("/", "/login","/css/**","/fonts/**","/images/**","/js/**","/aa/**"); //放行的请求
    }
}

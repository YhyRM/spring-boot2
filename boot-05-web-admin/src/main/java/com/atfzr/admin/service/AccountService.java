package com.atfzr.admin.service;

import com.atfzr.admin.bean.Account;

/**
 * @author silence
 * @date 2022/11/19---20:27
 * @SuppressWarnings({"all"})
 */
public interface AccountService {

      Account getAcctById(Long id);
}

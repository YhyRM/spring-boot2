package com.atfzr.admin.service;

import com.atfzr.admin.bean.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author silence
 * @date 2022/11/19---20:22
 * @SuppressWarnings({"all"})
 */
public interface UserService extends IService<User> {

}

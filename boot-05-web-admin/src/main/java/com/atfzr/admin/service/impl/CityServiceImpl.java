package com.atfzr.admin.service.impl;

import com.atfzr.admin.bean.City;
import com.atfzr.admin.mapper.CityMapper;
import com.atfzr.admin.service.CityService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author silence
 * @date 2022/11/19---15:46
 * @SuppressWarnings({"all"})
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityMapper cityMapper;

    Counter counter;

    public CityServiceImpl(MeterRegistry meterRegistry) {
    counter = meterRegistry.counter("cityService.saveCity.count");
    }

    @Override
    public  City getCityById(Long id){

      return   cityMapper.getById(id);
    }

    @Override
    public void saveCity(City city) {
        counter.increment();
        cityMapper.insert(city);
    }
}

package com.atfzr.admin.service.impl;

import com.atfzr.admin.bean.User;
import com.atfzr.admin.mapper.UserMapper;
import com.atfzr.admin.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author silence
 * @date 2022/11/19---20:23
 * @SuppressWarnings({"all"})
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


}

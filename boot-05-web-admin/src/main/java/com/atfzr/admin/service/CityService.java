package com.atfzr.admin.service;

import com.atfzr.admin.bean.City;

/**
 * @author silence
 * @date 2022/11/19---20:27
 * @SuppressWarnings({"all"})
 */
public interface CityService {
    public City getCityById(Long id);

    public void saveCity(City city);
}

package com.atfzr.admin.service.impl;

import com.atfzr.admin.bean.Account;
import com.atfzr.admin.mapper.AccountMapper;
import com.atfzr.admin.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author silence
 * @date 2022/11/19---14:27
 * @SuppressWarnings({"all"}) Controller(返回到浏览器,显示到页面)调用 Service(业务逻辑层) ，Service调用 mapper(接口，与sql配置文件的具体语句对应) DAO
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Override
    public  Account getAcctById(Long id){
//        实现接口的方法
        return  accountMapper.getAcct(id);
    }
}

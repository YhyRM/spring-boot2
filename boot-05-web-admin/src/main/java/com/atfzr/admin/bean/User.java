package com.atfzr.admin.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author silence
 * @date 2022/11/17---10:13
 * @SuppressWarnings({"all"})
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
//自定义表名
//@TableName("user_tbl")
public class User {
    /**
     *  所有属性都应该在数据库中
     */
    @TableField(exist = false)
    private  String userName;
    @TableField(exist = false)
    private  String password;

    /** ==============以下是数据库字段==============*/
    private  Long id;
    private  String name;
    private  Integer age;
    private  String email;

}

package com.atfzr.admin.bean;

import lombok.Data;

/**
 * @author silence
 * @date 2022/11/19---13:59
 * @SuppressWarnings({"all"})
 */
@Data
public class Account {

    private Long id;
    private String userId;
    private Integer money;
}

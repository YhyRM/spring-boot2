package com.atfzr.admin.bean;

import lombok.Data;

/**
 * @author silence
 * @date 2022/11/19---15:41
 * @SuppressWarnings({"all"})
 */
@Data
public class City {
    private Long id;
    private  String name;
    private  String state;
    private  String country;
}

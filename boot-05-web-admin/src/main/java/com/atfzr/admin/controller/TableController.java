package com.atfzr.admin.controller;

import com.atfzr.admin.bean.User;
import com.atfzr.admin.exception.UserTooManyException;
import com.atfzr.admin.service.UserService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.List;

/**
 * @author silence
 * @date 2022/11/17---11:12
 * @SuppressWarnings({"all"})
 */
@Controller
public class TableController {

    @Autowired
    UserService userService;

    /**
     *  @RequestParam("a") int a
     * @param a  不带请求参数或者 参数类型不对， 400; Bad Request  一般都是浏览器的参数没有传递正确
     * @return
     */
    @GetMapping("/basic_table")
    public String basic_table() {
//        int i = 10 / 0;
        return "table/basic_table";
    }

    @GetMapping("/user/delete/{id}")
    public  String deleteUser(@PathVariable("id") Long id,
                              @RequestParam(value = "pn",defaultValue = "1") Integer pn,
                              RedirectAttributes ra){

        userService.removeById(id);
        ra.addAttribute("pn",pn);

        return  "redirect:/dynamic_table";
    }

    @GetMapping("/dynamic_table")
    public String dynamic_table(@RequestParam(value = "pn",defaultValue = "1") Integer pn, Model model){
        //表格内容的遍历
//        response.sendError  会跳转到 4/5 xx页面
//        List<User> users = Arrays.asList(new User("zhangsan", "123456"),
//                new User("lisi", "123444"),
//                new User("haha", "aaaaa"),
//                new User("hehe ", "aaddd"));
//        model.addAttribute("users",users);

//        if(users.size() > 3){
        //自定义用户过多异常
//            throw  new UserTooManyException();
//        }

        List<User> list = userService.list();
        model.addAttribute("users",list);

        //分页查询的数据
        Page<User> userPage = new Page<>(pn, 2);

        //分页查询的结果
        Page<User> page = userService.page(userPage, null);
        long current = page.getCurrent();
        long pages = page.getPages();
        long total = page.getTotal();
        List<User> records = page.getRecords();

        model.addAttribute("page",page);

//        userService.getAll();

        return "table/dynamic_table";
    }

    @GetMapping("/responsive_table")
    public String responsive_table() {
        return "table/responsive_table";
    }

    @GetMapping("/editable_table")
    public String editable_table() {
        return "table/editable_table";
    }


}

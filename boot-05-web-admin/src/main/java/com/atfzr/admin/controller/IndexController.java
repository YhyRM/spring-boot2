package com.atfzr.admin.controller;

import com.atfzr.admin.bean.Account;
import com.atfzr.admin.bean.City;
import com.atfzr.admin.bean.User;
import com.atfzr.admin.service.AccountService;
import com.atfzr.admin.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @author silence
 * @date 2022/11/17---9:57
 * @SuppressWarnings({"all"})  完成登录 与 验证拦截功能
 */
@Controller
public class IndexController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    AccountService accountService;

    @Autowired
    CityService cityService;

    @ResponseBody
    @PostMapping("/city")
    // 因为要发post请求，为了方便就是用postman来进行测试(否则要建form表单)
    public City saveCity(City city){
        cityService.saveCity(city);
        return city;
    }

    //不
    @ResponseBody
    @GetMapping("/city")
    public City getCityById(@RequestParam("id") Long id) {
        return cityService.getCityById(id);
    }


    // 因为是返回json数据，而不是跳转页面 !!!
    @ResponseBody
    // 测试mybatis
    @GetMapping("/acct")
    public Account getById(@RequestParam("id") Long id) {


        return accountService.getAcctById(id);
    }

    @ResponseBody
    @GetMapping("/sql")
    public String queryFromDb() {
        Long aLong = jdbcTemplate.queryForObject("select count(*) from account_tbl", Long.class);
        return aLong.toString();
    }

    /**
     * 来登录页
     *
     * @return "/" 表示默认跳转路径
     */
    @GetMapping(value = {"/", "/login"})
    public String loginPage() {

        /*默认跳转到登录页面*/
        return "login";
    }

    @PostMapping("/login")
    public String main(User user, HttpSession session, Model model) { //RedirectAttributes

        if (StringUtils.hasLength(user.getUserName()) && "123456".equals(user.getPassword())) {
            //把登陆成功的用户保存起来
            session.setAttribute("loginUser", user);
            //登录成功重定向到main.html;  重定向防止表单重复提交
            return "redirect:/main.html";
        } else {
            model.addAttribute("msg", "账号密码错误");
            //回到登录页面
            return "login";
        }
    }

    /**
     * 去main页面
     *
     * @return
     */
    @GetMapping("/main.html")
    public String mainPage(HttpSession session, Model model) {

        //最好用拦截器,过滤器
        Object loginUser = session.getAttribute("loginUser");
        if (loginUser != null) {
            return "main";
        } else {
            //session过期，没有登陆过
            //回到登录页面
            model.addAttribute("msg", "请重新登录");
            return "login";
        }
    }

}


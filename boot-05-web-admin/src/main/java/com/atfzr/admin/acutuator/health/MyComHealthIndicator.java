package com.atfzr.admin.acutuator.health;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author silence
 * @date 2022/11/20---20:06
 * @SuppressWarnings({"all"})  指标监控--定制EndPoint(端点)
 *   组件名默认为: MyCom    , 也就是说 HealthIndicator是 自定义端点类的公共后缀！！！
 */
@Component
public class MyComHealthIndicator extends AbstractHealthIndicator {

    /**
     *  真实的检查方法
     * @param builder
     * @throws Exception
     */
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        // mongdb   获取连接进行测试
        HashMap<String, Object> map = new HashMap<>();
        // 检查完成
        if(true){
//            builder.up(); // 设置健康状态
            builder.status(Status.UP);
            map.put("count",1);
            map.put("ms",100);
        }else {
//            builder.down();
            builder.status(Status.OUT_OF_SERVICE);
            map.put("err","连接超时");
            map.put("ms",30000);
        }


        builder.withDetail("code",100)
                .withDetails(map);

    }
}

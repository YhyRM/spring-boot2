package com.atfzr.admin.acutuator.endpoint;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

/**
 * @author silence
 * @date 2022/11/20---20:49
 * @SuppressWarnings({"all"})  自定义端点
 */
@Component
@Endpoint(id = "container")
public class MyServiceEndPoint {
    @ReadOperation
    public Map getDockerInfo(){
        // 端点的读操作 http://localhost:8080/actuator/myservice
        return Collections.singletonMap("info","docker started...");
    }

    @WriteOperation
    private void restartDocker(){
        System.out.println("docker restarted....");
    }
}

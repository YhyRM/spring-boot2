package com.atfzr.admin.acutuator.info;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;

import java.util.Collections;

/**
 * @author silence
 * @date 2022/11/20---20:39
 * @SuppressWarnings({"all"})
 */
public class AppInfoInfoContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {

        builder.withDetail("msg","你好")
                .withDetail("hello","atfzr")
                .withDetails(Collections.singletonMap("world","666600"));
    }
}

package com.atfzr.hello.auto;

import com.atfzr.hello.bean.HelloProperties;
import com.atfzr.hello.service.HelloService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author silence
 * @date 2022/11/23---13:01
 * @SuppressWarnings({"all"})
 */
@Configuration
//没有 HelloService.class 才执行下面的, 所以应该写在方法上，写在类上当有该service类时，不会执行下面的配置类绑定功能
//@ConditionalOnMissingBean({HelloService.class})
// 开启配置类绑定功能  HelloProperties会自动跟  atfzr.hello 下的配置文件进行绑定
@EnableConfigurationProperties({HelloProperties.class})  // 默认HelloProperties 放在容器中
public class HelloServiceAutoConfiguration {

    @ConditionalOnMissingBean({HelloService.class})
    @Bean
    public HelloService helloService(){
        HelloService helloService = new HelloService();
        return helloService;
    }
}

package com.atfzr.hello.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author silence
 * @date 2022/11/23---12:56
 * @SuppressWarnings({"all"})
 */
@ConfigurationProperties("atfzr.hello")
public class HelloProperties {
    private String prefix;
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}

package com.atfzr.hello.service;

import com.atfzr.hello.bean.HelloProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author silence
 * @date 2022/11/23---12:53
 * @SuppressWarnings({"all"})
 */

/**
 * 默认不要放到容器中，  否则会报错
 */
public class HelloService {

    @Autowired
    HelloProperties helloProperties;

    public String sayHello(String userName){
        return helloProperties.getPrefix() + ":"  + userName + "》" + helloProperties.getSuffix();
    }
}

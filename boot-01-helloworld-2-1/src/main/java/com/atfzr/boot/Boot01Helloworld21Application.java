package com.atfzr.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot01Helloworld21Application {

    public static void main(String[] args) {
        SpringApplication.run(Boot01Helloworld21Application.class, args);
    }

}

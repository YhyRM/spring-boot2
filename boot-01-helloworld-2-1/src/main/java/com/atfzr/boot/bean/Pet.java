package com.atfzr.boot.bean;

import lombok.Data;
import lombok.ToString;

/**
 * @author silence
 * @date 2022/11/13---20:42
 * @SuppressWarnings({"all"})
 */
@Data
@ToString
public class Pet {
    private String name;
    private Double weight;
}

package com.atfzr.boot.controller;

import com.atfzr.boot.bean.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author silence
 * @date 2022/11/13---20:40
 * @SuppressWarnings({"all"})
 */
@RestController
public class HelloController {
    @Autowired
    Person person;

    @RequestMapping("/person")
    public Person person(){
        return person;
    }
}

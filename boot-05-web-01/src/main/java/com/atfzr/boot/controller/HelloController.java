package com.atfzr.boot.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @author silence
 * @date 2022/11/14---18:36
 * @SuppressWarnings({"all"})
 */
@RestController
public class HelloController {

    @RequestMapping("/运行截图2.jpg")
    public String hello(){
        return  "48!";
    }

    @GetMapping("/user")
//@RequestMapping(value = "/user",method = RequestMethod.GET)
    public String getUser(){
        return "GET-张三";
    }

    @PostMapping("/user")
//@RequestMapping(value = "/user",method = RequestMethod.POST)
    public String saveUser(){
        return "POST-张三";
    }

    @PutMapping("/user")
//@RequestMapping(value = "/user",method = RequestMethod.PUT)
    public String putUser(){
        return "PUT-张三";
    }

    @DeleteMapping("/user")
//@RequestMapping(value = "/user",method = RequestMethod.DELETE)
    public String deleteUser(){
        return "DELETE-张三";
    }
}

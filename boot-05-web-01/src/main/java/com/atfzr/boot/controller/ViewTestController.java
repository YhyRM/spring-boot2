package com.atfzr.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author silence
 * @date 2022/11/17---9:28
 * @SuppressWarnings({"all"})
 */
@Controller
public class ViewTestController {

    @GetMapping("/atfzr")
    public String atfzr(Model model){

        //model 中的数据会被放在请求域中 request.setAttribute("a",aa)
        model.addAttribute("msg","你好 fzr");
        model.addAttribute("link","https://www.baidu.com/");
        return "success";
    }
}

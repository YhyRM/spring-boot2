package com.atfzr.boot.controller;

import com.atfzr.boot.bean.Person;
import com.atfzr.boot.bean.Pet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * @author silence
 * @date 2022/11/16---18:32
 * @SuppressWarnings({"all"})
 */
@Controller
public class ResponseTestController {

    /**
     *@responsebody 这个注解表示你的返回值将存在responsebody中返回到前端，也就是将return返回值作为请求返回值，
     * return的数据不会解析成返回跳转路径，将java对象转为【json格式的数据】，前端接收后会显示将数据到页面，
     * 如果不加的话 返回值将会作为url的一部分，页面会跳转到这个url，也就是跳转到你返回的这个路径。
     *  给前端返回 json数据
     * @return
     */
    @ResponseBody
    @GetMapping("/test/person")
    public Person getPerson(){
        Person person = new Person();
        person.setUserName("张三");
        person.setAge(18);
        person.setBirth(new Date());
        return person;
    }
}

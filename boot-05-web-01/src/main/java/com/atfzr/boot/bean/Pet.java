package com.atfzr.boot.bean;

import lombok.Data;

/**
 * @author silence
 * @date 2022/11/16---15:06
 * @SuppressWarnings({"all"})
 */
@Data
public class Pet {
    private String name;
    private String age;
}

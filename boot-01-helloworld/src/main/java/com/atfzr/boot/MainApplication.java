package com.atfzr.boot;

import ch.qos.logback.core.db.DBHelper;
import com.atfzr.boot.bean.Pet;
import com.atfzr.boot.bean.User;
import com.atfzr.boot.config.MyConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.cache.interceptor.CacheAspectSupport;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author silence
 * @date 2022/11/13---9:54
 * @SuppressWarnings({"all"})
 */

/**
 * @SpringBootApplication: 告诉程序这是一个springboot应用
 */
@SpringBootApplication
public class MainApplication {
    public static void main(String[] args) {
        //使主程序类运行起来
        //1、 返回我们IOC容器
        ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);

        //2、 查看容器里的组件
        String[] names = run.getBeanDefinitionNames();
        for(String name : names){
            System.out.println(name);
        }

        int beanDefinitionCount = run.getBeanDefinitionCount();
        System.out.println("容器中有 " +  beanDefinitionCount + "个组件");

        String[] beanNamesForType = run.getBeanNamesForType(CacheAspectSupport.class);
        System.out.println("=====" + beanNamesForType.length);

        String[] beanNamesForType1 = run.getBeanNamesForType(WebMvcProperties.class);
        System.out.println("=====" + beanNamesForType1.length );

        //3、 从容器中获取组件
//        Pet tom = run.getBean("tom", Pet.class);
//
//        Pet tom1 = run.getBean("tom", Pet.class);
//
//        System.out.println("组件：" + (tom == tom1));
//
//        //4、 com.atfzr.boot.config.MyConfig$$EnhancerBySpringCGLIB$$746ee69c@606fc505
//        MyConfig bean = run.getBean(MyConfig.class);
//        System.out.println(bean);
//
//        // 如果@Configuration(proxyBeanMethods = true) 代理对象调用方法。SpringBoot总会检查这个组件是否在容器中
//        // 保持组件单实例
//        User user = bean.user01();
//        User user1 = bean.user01();
//        // 这为true说明 组件的方法也是从容器中取的
//        System.out.println(user == user1);
//
//        User user01 = run.getBean("user01", User.class);
//        Pet tom2 = run.getBean("tom", Pet.class);
//        System.out.println("用户的宠物:" + (user01.getPet() == tom2));
//
//        //5、 获取组件
//        String[] beanNamesForType = run.getBeanNamesForType(User.class);
//
//        for(String s : beanNamesForType){
//            // 输出beanId
//            System.out.println(s);
//        }
//
//        DBHelper bean1 = run.getBean(DBHelper.class);
//        System.out.println(bean1);

        boolean tom = run.containsBean("tom");
        System.out.println("容器中是否存在tom组件：" + tom); //false

        boolean user01 = run.containsBean("user01");
        System.out.println("容器中是否存在user01组件：" + user01); //true

        boolean tom22 = run.containsBean("tom22");
        System.out.println("容器中是否存在tom22组件：" + tom22); //true


        boolean haha = run.containsBean("haha");
        boolean hehe = run.containsBean("hehe");
        System.out.println("haha=" + haha  );
        System.out.println("hehe=" + hehe );
    }
}

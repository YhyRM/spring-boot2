package com.atfzr.boot.config;

import ch.qos.logback.core.db.DBHelper;
import com.atfzr.boot.bean.Car;
import com.atfzr.boot.bean.Pet;
import com.atfzr.boot.bean.User;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 * @author silence
 * @date 2022/11/13---14:00
 * @SuppressWarnings({"all"})
 */

/**
 *  1. 配置类里面使用@Bean 标注在方法上给容器注册组件，默认也是【单实例】
 *  2. 配置类本身也是组件
 *  3. proxyBeanMethods: 代理bean的方法
 *      Full(proxyBeanMethods = true), （保证每个@Bean方法被调用多少次返回的组件都是单实例的）（默认） 同一个方法不管调用几次始终为同一个对象
 *      Lite(proxyBeanMethods = false)  （每个@Bean方法被调用多少次返回的组件都是新创建的）每次调用方法都会new一个对象出来
 *      组件依赖必须使用Full模式默认。 其他默认是否Lite模式
 *
 *  4、 @Import({User.class, DBHelper.class})
 *     给容器中 自动创建出这两个类型的组件(Bean), 、默认组件的名字就是全类名
 *     而 @autowire是给【容器中的bean】注入属性(自动装配，依赖注入)
 *
 *  5、 @ImportResource("classpath:beans.xml") 导入 Spring配置文件，使它生效
 *
 */

@Import({User.class, DBHelper.class})
@Configuration(proxyBeanMethods = true) //告诉SpringBoot 这是一个配置类 == 配置文件（xml）
//@ConditionalOnBean(name = "tom")  //与下面相反
@ConditionalOnMissingBean(name =  "tom")  //没有tom名字的Bean时，MyConfig类的Bean才能生效。
// classpath:(类路径，从resources开始算)
@ImportResource("classpath:beans.xml")

// 1、开启Car 配置绑定功能
// 2、把Car 这个组件自动注册到容器中
@EnableConfigurationProperties(Car.class)
public class MyConfig {

    /** ===外部无论对配置类中的这个组件注册方法调用多少次获取的都是之前注册容器中的单实例对象（代理中介,代理模式）==
     * @return User
     * bean id =user01，  也可以在Bean标签括号里自定义beanId
     * @Bean 给容器中添加组件。 以【方法名】作为组件的id。返回类型就是组件类型。 返回的值就是组件在容器中的实例
     */
    @Bean
    public User user01() {
        //构造器初始化参数
//        return new User("fzr", 20);
        User fzr = new User("fzr", 20);
        // user组件依赖了Pet组件
        fzr.setPet(tomcatPet());
        return fzr;
    }

    @Bean("tom22")
    public Pet tomcatPet() {
        return new Pet("tomcat");
    }
}

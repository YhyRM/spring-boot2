package com.atfzr.boot.controller;

import com.atfzr.boot.bean.Car;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author silence
 * @date 2022/11/13---9:58
 * @SuppressWarnings({"all"})
 */

/**
 * 因为要返回字符串，所以 加上@ResponseBody，
 * 否则 return这个会被解析成页面，但是你找不到页面文件，会报错
 *
 * @return 写在类上表示加在类中的每一个方法
 */
//@ResponseBody
//@Controller
// 这是好几个注解的 合并
@Slf4j
@RestController
public class HelloController {

    @Autowired
    Car car;

    @RequestMapping("/car")
    public Car car() {
        return car;
    }


    // 浏览器导航栏路径
    @RequestMapping("/hello")
    public String handle01(@RequestParam("name") String name) {
        log.info("请求进来了");
        return "Hello, Spring Boot 2!" + "你好: " + name;
    }
}

package com.atfzr.boot.bean;

/**
 * @author silence
 * @date 2022/11/13---15:42
 * @SuppressWarnings({"all"})
 */

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 *  只有在容器中的组件，才会拥有SpringBoot 提供的强大功能
 */
//========lombok==========
@ToString
@Data
//========lombok==========
@Component //注册组件
@ConfigurationProperties(prefix = "mycar") //与核心配置文件application.properties 进行绑定，一般与@Component混用(如果该类没有注册为组件的话)
public class Car {
    private  String brand;
    private  Integer price;


}

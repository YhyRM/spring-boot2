package com.atfzr.boot.bean;

import lombok.*;

/**
 * @author silence
 * @date 2022/11/13---13:49
 * @SuppressWarnings({"all"})  用户
 */
@ToString
@Data
@NoArgsConstructor //无参构造器
//@AllArgsConstructor //全参构造器
@EqualsAndHashCode
public class User {
    private  String name;
    private  Integer age;

    private Pet pet;

    public User(String name,Integer age){
        this.name = name;
        this.age = age;
    }


}

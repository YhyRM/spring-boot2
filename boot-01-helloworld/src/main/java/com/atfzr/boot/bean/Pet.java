package com.atfzr.boot.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author silence
 * @date 2022/11/13---13:52
 * @SuppressWarnings({"all"})  宠物
 */
@ToString
@Data
@NoArgsConstructor //无参构造器
@AllArgsConstructor //全参构造器
public class Pet {
    private  String name;




}

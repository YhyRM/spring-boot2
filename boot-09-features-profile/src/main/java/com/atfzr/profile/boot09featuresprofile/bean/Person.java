package com.atfzr.profile.boot09featuresprofile.bean;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author silence
 * @date 2022/11/22---18:59
 * @SuppressWarnings({"all"})
 */
//@Data
//// 将实例加到组件中
//@Component
    //以person的形式绑定配置文件
//@ConfigurationProperties("person")
public interface Person {
//    private String name;
//    private Integer age;
}

package com.atfzr.profile.boot09featuresprofile.controller;

import com.atfzr.profile.boot09featuresprofile.bean.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.PipedWriter;

/**
 * @author silence
 * @date 2022/11/22---18:41
 * @SuppressWarnings({"all"})
 */

@RestController
public class HelloController {

//    如果配置文件里没有就默认李四
    @Value("${person.name:李四}")
    private String name;

    @Autowired
   private Person person;

    @Value("${MAVEN_HOME}")
    private String msg;

    @Value("${os.name}")
    private String osName;

    @RequestMapping("/")
    public String hello(){
        return  person.getClass().toString();
    }

    @RequestMapping("/person")
    public Person person(){
        return  person;
    }

    @RequestMapping("/msg")
    public String msg(){
        return  msg + "==>" + osName ;
    }
}

package com.atfzr.profile.boot09featuresprofile.config;

import com.atfzr.profile.boot09featuresprofile.bean.Color;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author silence
 * @date 2022/11/22---19:15
 * @SuppressWarnings({"all"})
 */
@Configuration
public class MyConfig {

    //这个激活注解也可以添加在方法上！！
    @Profile("prod")
    @Bean
    public Color red(){
        return new Color();
    }

    @Profile("test")
    @Bean
    public Color green(){
        return new Color();
    }

}

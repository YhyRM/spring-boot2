package com.atfzr.boot.controller;

import com.atfzr.hello.auto.HelloServiceAutoConfiguration;
import com.atfzr.hello.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author silence
 * @date 2022/11/23---13:25
 * @SuppressWarnings({"all"})
 */
@RestController
public class HelloController {

   @Autowired
    HelloService helloService;

//   HelloServiceAutoConfiguration

   @GetMapping("/hello")
    public String  sayHello(){
        String regStr = helloService.sayHello("张三");
        return  regStr;

    }
}

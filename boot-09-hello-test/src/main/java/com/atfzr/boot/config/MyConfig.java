package com.atfzr.boot.config;

import com.atfzr.hello.service.HelloService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author silence
 * @date 2022/11/23---14:20
 * @SuppressWarnings({"all"})
 */
@Configuration
public class MyConfig {

    @Bean
    public HelloService helloService(){
        HelloService helloService = new HelloService();
        return helloService;
    }
}
